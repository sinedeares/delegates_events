﻿using System;
using NUnit.Framework;
using Timer.Factories;

namespace Timer.Tests
{
	[TestFixture]
	public class TimerTests
	{
		[TestCase("")]
		[TestCase(null)]
		public void Ctor_EmptyTimerName_ThrowsArgumentException(string timerName)
		{
			var timerFactory = GetTimerFactory();

			Assert.Throws<ArgumentException>(() => timerFactory.CreateTimer(timerName, 1));
		}

		[TestCase(0)]
		[TestCase(-1)]
		[TestCase(int.MinValue)]
		public void Ctor_InvalidSecondsCount_ThrowsArgumentException(int totalTicks)
		{
			var timerFactory = GetTimerFactory();

			Assert.Throws<ArgumentException>(() => timerFactory.CreateTimer("tea", totalTicks));
		}

		private TimerFactory GetTimerFactory()
		{
			return new TimerFactory();
		}
	}
}