﻿using System;

namespace Timer.Factories
{
	public class TimerFactory
	{
		public Timer CreateTimer(string name, int ticks)
		{
			if (!String.IsNullOrEmpty(name) && ticks > 0)
			{
				return new Timer(name, ticks);
			}
			throw new ArgumentException("Имя пустое или количество тиков меньше или рано нулю");
			//throw new NotImplementedException();
		}
	}
}