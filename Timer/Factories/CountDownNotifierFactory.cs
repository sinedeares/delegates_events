﻿using System;
using Timer.Implementation;
using Timer.Interfaces;

namespace Timer.Factories
{
	public class CountDownNotifierFactory
	{
		public ICountDownNotifier CreateNotifierForTimer(Timer timer)
		{
			if (timer != null){
				return new CountDownNotifier(timer);
			}
			throw new ArgumentNullException("Таймер отсутствует");
			//throw new NotImplementedException();
		}
	}
}
