﻿using System;
using System.Threading;
using Timer.Interfaces;

namespace Timer.Implementation
{
	public class CountDownNotifier : ICountDownNotifier
	{
		public Timer timer;
		public CountDownNotifier(Timer timer)
        {
			this.timer = timer;
        }
		public event Action<string, int> Started = delegate { };
		public event Action<string> Stopped = delegate { };
		public event Action<string, int> Tick = delegate { };

		public void Init(Action<string, int> startDelegate, Action<string> stopDelegate, Action<string, int> tickDelegate)
		{
			if (startDelegate != null)
			{
				Started += startDelegate;
			}

			if (tickDelegate != null)
			{
				Tick += tickDelegate;
			}

			if (stopDelegate != null)
			{
				Stopped += stopDelegate;
			}
		
			//throw new NotImplementedException();
		}

		public void Run()
		{
			Started.Invoke(timer.name, timer.ticks);
			for (var tick = timer.ticks-1; tick > 0; tick--)
            {
				Thread.Sleep(50);
				Tick.Invoke(timer.name, tick);
            }
			Stopped.Invoke(timer.name);
			//throw new NotImplementedException();
		}
	}
}