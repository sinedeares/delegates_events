﻿namespace Timer
{
	public class Timer
	{
		public string name;
		public int ticks;

		public Timer(string name, int ticks)
        {
			this.name = name;
			this.ticks = ticks;

        }
	}
}